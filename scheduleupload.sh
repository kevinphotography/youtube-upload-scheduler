#!/bin/sh
# Title: YouTube Upload Scheduler
# Summary: Script to schedule uploading video file to YouTube
# 

YOUTUBEDEBUG=1
YOUTUBECREDENTIALS=null
YOUTUBECLIENTID=null
echo "######################################################"
echo "            Youtube Schedule Upload Tool"
echo "######################################################"
if [ -f FIRSTRUN ]
	then
		echo "It looks like it is your first time running, we need to install some things before using this sript."
		echo "Note: you will need to have sudo access or root privilages to install things"
		echo "-----------------------------------------------------"
		echo "                Installing dependencies"
		echo "-----------------------------------------------------"
		echo "-----------------------------------------------------"
		echo "                   Python installed?"
		echo "-----------------------------------------------------"
		echo "Do you have python 2.6 or 2.7 installed?"
		echo "Note: Python 3.0+ is not supported here. "
		echo "1) Yes"
		echo "2) No"
		echo -n "(1) Yes/(2) No: "
		read YouTubeInstallQuestion
		if [[ ${YouTubeInstallQuestion:0:1} == "2" ]]
			then
				mkdir YouTube
				cd YouTube
				sudo apt-get update
				wait
				# Install Python 2.7.5
				sudo apt-get -y install build-essential
				sudo apt-get -y install libreadline-gplv2-dev libncurseswsudo 5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
				wget http://python.org/ftp/python/2.7.3/Python-2.7.3.tgz
				wait
				tar -xvf Python-2.7.3.tgz
				wait
				cd Python-2.7.3
				./configure
				make
				sudo make install
				cd ..
				#install Pip
				sudo apt-get -y install python-pip
				#install Google Python API
				sudo pip install oauth2client
				sudo pip install apiclient
				sudo pip install httplib2
				sudo pip install uritemplate
				sudo pip install python-gflags
				sudo pip install --upgrade google-api-python-client progressbar
				#install Tokland's Youtube-upload
				sudo apt-get -y install unzip
				wget https://github.com/tokland/youtube-upload/archive/master.zip
				wait
				unzip master.zip
				cd youtube-upload-master
				sudo python setup.py install
				cd ..
				#cleanup
				rm Python-2.7.5.tgz
				rm -r Python-2.7.5
		else
				echo "-----------------------------------------------------"
				echo "                   Pip Installed?"
				echo "-----------------------------------------------------"
				echo "Do you have Pip installed? "
				echo "1) Yes"
				echo "2) No"
				echo -n "(1) Yes/(2) No: "
				read YouTubeInstallQuestion
				if [[ ${YouTubeInstallQuestion:0:1} == "2" ]]
					then
						#install Pip
						sudo apt-get -y install python-pip
						#install Google Python API
						pip install --upgrade google-api-python-client progressbar
						#install Tokland's Youtube -upload
						wget https://github.com/tokland/youtube-upload/archive/master.zip
						unzip master.zip
						cd youtube-upload-master
						sudo python setup.py install
				else
					echo "-----------------------------------------------------"
					echo "               Google Python Installed?"
					echo "-----------------------------------------------------"
					echo "Do you have Google Python API installed? "
					echo "1) Yes"
					echo "2) No"
					echo -n "(1) Yes/(2) No: "
					read YouTubeInstallQuestion
					if [[  ${YouTubeInstallQuestion:0:1} == "2" ]]
						then
							#install Google Python API
							pip install --upgrade google-api-python-client progressbar
							#install Tokland's Youtube -upload
							wget https://github.com/tokland/youtube-upload/archive/master.zip
							unzip master.zip
							cd youtube-upload-master
							sudo python setup.py install
					else
						echo "-----------------------------------------------------"
						echo "              youtube-upload Tokland?"
						echo "-----------------------------------------------------"
						echo "Do you have Tokland's Youtube upload python script installed? "
						echo "Note: From https://github.com/tokland/youtube-upload"
						echo "1) Yes"
						echo "2) No"
						echo -n "(1) Yes/(2) No: "
						read YouTubeInstallQuestion
						if [[ ${YouTubeInstallQuestion:0:1} == "2" ]]
							then
								#install Tokland's Youtube -upload
								wget https://github.com/tokland/youtube-upload/archive/master.zip
								unzip master.zip
								cd youtube-upload-master
								sudo python setup.py install
						fi
					fi
				fi
		fi
		rm FIRSTRUN.sh
		echo "######################################################"
		echo "Basicly everthing needed has been installed."
		echo "Note: while Tokland provided a API JSON file to upload to YouTube, you should get your own API JSON key. It is free and easy to get (Just place it in the same folder as this script and/or point the JSON file in the heading of the script, on line 5, after YOUTUBECREDENTIALS=" 
		echo "Follow https://developers.google.com/youtube/registering_an_application for more information. "
		echo ""
		echo "You should run the quickupload.sh script once and follow the instructions to set up authentication to your YouTube account."
		echo "Note: If you have multiple YouTube channels you want to upload, see Tokland's instruction on doing that."
else
		if [ $# -eq 0 ]
			then
				echo "No Video Supplied, Quitting"
				echo "./scheduleupload.sh (Video Filename Here).(Video Extension Here)"
				echo "or"
				echo "sh scheduleupload.sh (Video Filename Here).(Video Extension Here)"
		else 
				echo "-----------------------------------------------------"
				echo "              Setting YouTube Information"
				echo "-----------------------------------------------------"
				echo -n "Scheduling Video for "
				echo $1
				echo -n "Please Enter the title of the video: "
				read YouTubeTitle
				echo "Please enter a description of the video."
				echo -n "> "
				read YouTubeDescription
				echo -n "Please Enter the Category of the video: "
				read YouTubeCategory
				YouTubeCategoryDefined=Entertainment
				# A = 7 Checked
				#	Animation
				#	Autos
				#	Anime
				#	Animals
				#	Animation
				#	Action
				#	Adventure
				if [[ ${YouTubeCategory:0:1} == "a" ]] || [[ ${YouTubeCategory:0:1} == "A" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "n" ]] || [[ ${YouTubeCategory:1:1} == "N" ]]
							then
								if [[ ${YouTubeCategory:4:1} == "e" ]] || [[ ${YouTubeCategory:4:1} == "E" ]]
									then
										YouTubeCategoryDefined="Anime/Animation"
								fi
								if [[ ${YouTubeCategory:4:1} == "a" ]] || [[ ${YouTubeCategory:4:1} == "A" ]]
									then
										if [[ ${YouTubeCategory:5:1} == "l" ]] || [[ ${YouTubeCategory:5:1} == "L" ]]
											then
												YouTubeCategoryDefined="Pets & Animals"
										fi
										if [[ ${YouTubeCategory:5:1} == "t" ]] || [[ ${YouTubeCategory:5:1} == "A" ]]
											then
												YouTubeCategoryDefined="Anime/Animation"
										fi
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "c" ]] || [[ ${YouTubeCategory:1:1} == "C" ]]
							then
								if [[ ${YouTubeCategory:4:1} == "o" ]] || [[ ${YouTubeCategory:4:1} == "O" ]]
									then
										YouTubeCategoryDefined="Action/Adventure"
								fi
								if [[ ${YouTubeCategory:4:1} == "v" ]] || [[ ${YouTubeCategory:4:1} == "V" ]]
									then
										YouTubeCategoryDefined="Nonprofits & Activism"
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "d" ]] || [[ ${YouTubeCategory:1:1} == "D" ]]
							then
								YouTubeCategoryDefined="Action/Adventure"
						fi
						if [[ ${YouTubeCategory:1:1} == "u" ]] || [[ ${YouTubeCategory:1:1} == "U" ]]
							then
								YouTubeCategoryDefined="Autos & Vehicles"
						fi

				fi
				#B = 1 Checked
				#	Blogs
				if [[ ${YouTubeCategory:0:1} == "b" ]] || [[ ${YouTubeCategory:0:1} == "B" ]]
					then
						YouTubeCategoryDefined="People & Blogs"
				fi
				#C = 2 Checked
				#	Comedy
				#	Classics
				if [[ ${YouTubeCategory:0:1} == "c" ]] || [[ ${YouTubeCategory:0:1} == "C" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "O" ]]
							then
								YouTubeCategoryDefined="Comedy"
						fi
						if [[ ${YouTubeCategory:1:1} == "l" ]] || [[ ${YouTubeCategory:1:1} == "L" ]]
							then
								YouTubeCategoryDefined="Classics"
						fi
				fi
				#D = 2 Checked
				#	Documentary
		    	#	Drama
				if [[ ${YouTubeCategory:0:1} == "d" ]] || [[ ${YouTubeCategory:0:1} == "D" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "O" ]]
							then
								YouTubeCategoryDefined="Documentary"
						fi
						if [[ ${YouTubeCategory:1:1} == "r" ]] || [[ ${YouTubeCategory:1:1} == "R" ]]
							then
								YouTubeCategoryDefined="Drama"
						fi
				fi
				#E = 2 Checked
				#	Events
				#	Entertainment
				#	Education
				if [[ ${YouTubeCategory:0:1} == "e" ]] || [[ ${YouTubeCategory:0:1} == "E" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "d" ]] || [[ ${YouTubeCategory:1:1} == "D" ]]
							then
								YouTubeCategoryDefined=Education
						fi
						if [[ ${YouTubeCategory:1:1} == "n" ]] || [[ ${YouTubeCategory:1:1} == "N" ]]
							then
								YouTubeCategoryDefined=Entertainment
						fi
						if [[ ${YouTubeCategory:1:1} == "v" ]] || [[ ${YouTubeCategory:1:1} == "V" ]]
							then
								YouTubeCategoryDefined="Travel & Events"
						fi
				fi
				#F = 3 Checked
				#	Film
				#	Family
				#	Foreign
				#	Fantasy
				if [[ ${YouTubeCategory:0:1} == "f" ]] || [[ ${YouTubeCategory:0:1} == "F" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "i" ]] || [[ ${YouTubeCategory:1:1} == "I" ]]
							then
								YouTubeCategoryDefined="Film & Animation"
						fi
						if [[ ${YouTubeCategory:1:1} == "a" ]] || [[ ${YouTubeCategory:1:1} == "A" ]]
							then
								if [[ ${YouTubeCategory:2:1} == "n" ]] || [[ ${YouTubeCategory:2:1} == "N" ]]
									then
										YouTubeCategoryDefined=Sci-Fi/Fantasy
								fi
								if [[ ${YouTubeCategory:2:1} == "m" ]] || [[ ${YouTubeCategory:2:1} == "M" ]]
									then
										YouTubeCategoryDefined=Family
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "O" ]]
							then
								YouTubeCategoryDefined=Foreign
						fi
				fi
				#G = 1 Checked
				#	Gaming
				if [[ ${YouTubeCategory:0:1} == "g" ]] || [[ ${YouTubeCategory:0:1} == "G" ]]
					then
						YouTubeCategoryDefined=Gaming
				fi
				#H =  2 Checked
				#	Horror
				#	Howto
				if [[ ${YouTubeCategory:0:1} == "h" ]] || [[ ${YouTubeCategory:0:1} == "H" ]]
					then
						if [[ ${YouTubeCategory:2:1} == "r" ]] || [[ ${YouTubeCategory:2:1} == "R" ]]
							then
								YouTubeCategoryDefined=Horror
						fi
						if [[ ${YouTubeCategory:2:1} == "w" ]] || [[ ${YouTubeCategory:2:1} == "W" ]]
							then
								YouTubeCategoryDefined="Howto & Style"
						fi
				fi
				#N = 2 Checked
				#	News
				#	NonProfits
				if [[ ${YouTubeCategory:0:1} == "n" ]] || [[ ${YouTubeCategory:0:1} == "N" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "o" ]]
							then
								YouTubeCategoryDefined="Nonprofits & Activism"
						fi
						if [[ ${YouTubeCategory:1:1} == "e" ]] || [[ ${YouTubeCategory:1:1} == "E" ]]
							then
								YouTubeCategoryDefined="News & Politics"
						fi
				fi
				#M = 2 Checked
				#	Movies
				#	Music
				if [[ ${YouTubeCategory:0:1} == "m" ]] || [[ ${YouTubeCategory:0:1} == "M" ]]
					then
						YouTubeCategoryDefined=Music
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "O" ]]
							then
								YouTubeCategoryDefined="Movies"
						fi
						if [[ ${YouTubeCategory:1:1} == "u" ]] || [[ ${YouTubeCategory:1:1} == "U" ]]
							then
								YouTubeCategoryDefined="Music"
						fi
				fi
				#P = 3 Checked
				#	Pets
				#	People
				#	Politics
				if [[ ${YouTubeCategory:0:1} == "p" ]] || [[ ${YouTubeCategory:0:1} == "P" ]]
					then
						YouTubeCategoryDefined=Pets
						if [[ ${YouTubeCategory:2:1} == "o" ]] || [[ ${YouTubeCategory:2:1} == "O" ]]
							then
								YouTubeCategoryDefined="People & Blogs"
						fi
						if [[ ${YouTubeCategory:2:1} == "t" ]] || [[ ${YouTubeCategory:2:1} == "T" ]]
							then
								YouTubeCategoryDefined="Pets & Animals"
						fi
						if [[ ${YouTubeCategory:1:1} == "o" ]] || [[ ${YouTubeCategory:1:1} == "O" ]]
							then
								YouTubeCategoryDefined="News & Politics"
						fi
				fi
				#S = 6 Checked
				#	Sports
				#	Short Movies
				#	Science
				#	Sci-Fi
				#	Shorts
				#	Shows
				if [[ ${YouTubeCategory:0:1} == "s" ]] || [[ ${YouTubeCategory:0:1} == "S" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "c" ]] || [[ ${YouTubeCategory:1:1} == "C" ]]
							then
								YouTubeCategoryDefined=Science
								if [[ ${YouTubeCategory:3:1} == "-" ]]
									then
										YouTubeCategoryDefined="Sci-Fi/Fantasy"
								fi
								if [[ ${YouTubeCategory:3:1} == "e" ]] || [[ ${YouTubeCategory:2:1} == "E" ]]
									then
										YouTubeCategoryDefined="Science"
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "h" ]] || [[ ${YouTubeCategory:2:1} == "I" ]]
							then
								YouTubeCategoryDefined=Shows
								if [[ ${YouTubeCategory:5:1} == " " ]]
									then
										YouTubeCategoryDefined="Short Movies"
								fi
								if [[ ${YouTubeCategory:5:1} == "s" ]] || [[ ${YouTubeCategory:5:1} == "S" ]]
									then
										YouTubeCategoryDefined="Shorts"
								fi
								if [[ ${YouTubeCategory:4:1} == "s" ]] || [[ ${YouTubeCategory:4:1} == "S" ]]
									then
										YouTubeCategoryDefined="Shows"
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "p" ]] || [[ ${YouTubeCategory:1:1} == "P" ]]
							then
								YouTubeCategoryDefined="Sports"
						fi
				fi
				#T = 4 Checked
				#	Travel
				#	Technology
				#	Thriller
				#	Trailers
				if [[ ${YouTubeCategory:0:1} == "t" ]] || [[ ${YouTubeCategory:0:1} == "T" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "r" ]] || [[ ${YouTubeCategory:1:1} == "R" ]]
							YouTubeCategoryDefined=Travel
							then
								if [[ ${YouTubeCategory:3:1} == "i" ]] || [[ ${YouTubeCategory:3:1} == "I" ]]
									then
										YouTubeCategoryDefined="Trailers"
								fi
								if [[ ${YouTubeCategory:3:1} == "v" ]] || [[ ${YouTubeCategory:3:1} == "V" ]]
									then
										YouTubeCategoryDefined="Travel"
								fi
						fi
						if [[ ${YouTubeCategory:1:1} == "h" ]] || [[ ${YouTubeCategory:1:1} == "H" ]]
							then
								YouTubeCategoryDefined="Thriller"
						fi
						if [[ ${YouTubeCategory:1:1} == "e" ]] || [[ ${YouTubeCategory:1:1} == "E" ]]
							then
								YouTubeCategoryDefined="Science & Technology"
						fi
				fi
				#V = 2 CHECKED
				#	Vehicles
				#	Videoblogging
				if [[ ${YouTubeCategory:0:1} == "v" ]] || [[ ${YouTubeCategory:0:1} == "V" ]]
					then
						if [[ ${YouTubeCategory:1:1} == "e" ]] || [[ ${YouTubeCategory:1:1} == "E" ]]
							then
								YouTubeCategoryDefined="Autos & Vehicles"
						fi
						if [[ ${YouTubeCategory:1:1} == "i" ]] || [[ ${YouTubeCategory:1:1} == "I" ]]
							then
								YouTubeCategoryDefined="Videoblogging"
						fi
				fi
				if [ $YOUTUBEDEBUG -eq 1 ]
					then
						echo -n "Category: "
						echo $YouTubeCategoryDefined
				fi
				echo -n "Please Enter the tags (seperated by \", \"): "
				read YouTubeTags
				echo "-----------------------------------------------------"
				echo "              Setting up time to upload"
				echo "-----------------------------------------------------"
				echo "Time can be in the following:"
				echo "     11:59 PM"
				echo "     2359"
				echo "     23:59"
				echo "     noon"
				echo "     now"
				echo "(Note: Times are in System Locale Set Time)"
				echo -n "Time for the video file to upload: "
				read YouTubeTime
				if [ "$YouTubeTime" == "now" ] || [ "$YouTubeTime" == "Now" ] || [ "$YouTubeTime" == "NOW" ]
					then
						youtube-upload --title="$YouTubeTitle" --description="$YouTubeDescription" --category="$YouTubeCategory" --tags="$YouTubeTags" $1
				else
						echo "Date can be in the following:"
						echo "     31.12.2015"
						echo "     December 31"
						echo "     Sunday"
						echo "     tomorow"
						echo "     next month"
						echo -n "Please Enter the date of upload: "
						read YouTubeDate
						echo "----------Processing data into script---------"
						printf "youtube-upload --title=\"" > $1.sh
						echo -n $YouTubeTitle >> $1.sh
						printf "\" --description=\"" >> $1.sh
						echo -n $YouTubeDescription >> $1.sh
						printf "\" --category=" >> $1.sh
						echo -n $YouTubeCategory >> $1.sh
						printf " --tags=\"" >> $1.sh
						echo -n $YouTubeTags >> $1.sh
						printf "\" " >> $1.sh
						if [ ! "$YOUTUBECLIENTID" == "null" ]
							then
								echo -n "--client-secrets=" >> $1.sh
								echo -n $YOUTUBECLIENTID >> $1.sh
								printf " " >> $1.sh
						fi
						if [ ! "$YOUTUBECREDENTIALS" == "null" ]
							then
								echo -n "--credentials-file=" >> $1.sh
								echo -n $YOUTUBECREDENTIALS >> $1.sh
								printf " " >> $1.sh
						fi
						echo -n $1 >> $1.sh
						printf "\n" >> $1.sh
						printf "if [ ! -d done ];\n" >> $1.sh
						printf "	then\n" >> $1.sh
						echo "		mkdir done" >> $1.sh
						echo "fi" >> $1.sh
						echo -n "mv " >> $1.sh
						echo -n $1 >> $1.sh
						printf " done/" >> $1.sh
						echo -n $1 >> $1.sh
						printf "\n" >> $1.sh
						printf "if [ -f " >> $1.sh
						echo -n $1.sh >> $1.sh
						printf " ]\n" >> $1.sh
						printf "	then\n" >> $1.sh
						printf "		mv " >> $1.sh
						echo -n $1 >> $1.sh
						printf ".sh done/" >> $1.sh
						echo -n $1 >> $1.sh
						printf ".sh\n" >> $1.sh
						printf "fi\n" >> $1.sh
						printf "exit\n" >> $1.sh
						chmod +x $1.sh
						echo "----------Script Created---------"
						echo "----------Scheduling Script---------"
						at -f ./$1.sh $YouTubeTime $YouTubeDate
						if [ $YOUTUBEDEBUG -eq 0 ]
							then
								rm ./$1.sh
						else
								echo -n ""
						fi
				fi
		fi
fi
echo "#####################################################"
exit