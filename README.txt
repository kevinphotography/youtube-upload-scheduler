YouTube Upload Scheduler
Summary: Script to schedule upload video files to YouTube

YouTube Upload Scheduler is a simple script that will allow you to easily start a video to uploadto YouTube at a specific time. This is accomplished through the use of Tokland's YouTube Upload Python Script, YouTube's APIv3, and the at command avalable on Linux Debian operating system.

NOTE: Please see Tokland's YouTube Upload Authentication section for associating the python script with your account and a client_secrets.json. 

While this script was designed for the use on a server system running all of the time, this script can be used on a desktop or laptop, though the video will not upload if it cannot connnect to YouTube at the scheduled time. 

Keep in mind, depending on the size of the video, how much ram you have, your CPU load, your bandwith and YouTube/Google's load, the video may take a second to an hour to completely upload. Additionally, the time that the video will start uploading is the time corresponding to the system locale time.  

To set up the script, make the setup script executable.

chmod +x setup.sh

Then Run the Setup with the command 

./setup.sh

Then run the command with a video file in the same directory as the script.


./scheduleupload.sh <Video File Here>

Initially, the script will download all of the dependencies it needs (Tokland's YouTube Upload, Google APIv3, and at command) before asking you the things it needs to schedule a video upload. 

Tested Environment: Ubuntu 12.04LTS Server on a shared VPS with 2GB of allocated memory on a shared Intel Xeon Processor. 

Also included is a quick upload script used initially as a test to upload to YouTube in two steps. 

To Do
----------------------------------------------------------------
- Expand/refine the first run install of Tokland's YouTube Upload, Google APIv3, and at command.
- Advanced error handling. 
- Expand authentication json. 

Attribution: 
Tokland's YouTube Upload Script (https://github.com/tokland/youtube-upload)
Google YouTube APIv3 (https://developers.google.com/youtube/v3/)

Code is owned by Kevin Photography (kevinphotography.net) and is liscenced under the Creative Commons-ShareAlike 4.0 International (CC BY-SA 4.0), which means you are allowed to share the code and adapt this code into your own work as long as you give approperate attribution to the original code and that you must distribute this code under the same liscence from this code.

If you find any bugs in the code or would like to give some suggestion. Email contact@kevinphtography.net and place the tag "[YouTubeScheduleUpload]" in the subject line. 