#!/bin/bash

echo "######################################################"
echo "                Youtube Quick Upload Tool"
echo "######################################################"
if [ $# -eq 0 ]
	then
		echo "No Video Supplied, Quitting"
else 
		echo -n "Please Enter the title of the video: "
		read YouTubeTitle
		youtube-upload --title="$YouTubeTitle" $1
		mv $1 done/$1
fi

echo "#####################################################"
exit
